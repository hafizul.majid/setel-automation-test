import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class SetelAutoTest3{

    public static void main(String args[]) throws MalformedURLException{

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
        dc.setCapability("platformName","android");
        dc.setCapability("appPackage","com.todoist");
        dc.setCapability("appActivity",".activity.HomeActivity");
        dc.setCapability("noReset","true");

        AndroidDriver<AndroidElement> ad = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),dc);

        (new TouchAction(ad)).tap(PointOption.point(136, 129)).perform();
        (new TouchAction(ad)).tap(PointOption.point(688, 702)).perform();
        WebDriverWait w1 = new WebDriverWait(ad, 10);
        w1.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todoist:id/empty_content")));
        (new TouchAction(ad)).tap(PointOption.point(388, 879)).perform();
        w1.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todoist:id/empty_content")));
        MobileElement el1 = (MobileElement) ad.findElementByAccessibilityId("More options");
        el1.click();
        MobileElement el2 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[4]/android.widget.LinearLayout");
        el2.click();
        MobileElement el3 = (MobileElement) ad.findElementByXPath("(//android.widget.CheckBox[@content-desc=\"Complete\"])[1]");
        el3.click();

    }
}
