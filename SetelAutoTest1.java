import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.net.MalformedURLException;

public class SetelAutoTest1{

    public static void main(String args[]) throws MalformedURLException{

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
        dc.setCapability("platformName","android");
        dc.setCapability("appPackage","com.todoist");
        dc.setCapability("appActivity",".activity.HomeActivity");
        dc.setCapability("noReset","true");

        AndroidDriver<AndroidElement> ad = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),dc);

        (new TouchAction(ad)).tap(PointOption.point(133, 140)).perform();
        (new TouchAction(ad)).tap(PointOption.point(784, 753)).perform();

        //stale element exception handling
        for(int i=0; i<=2;i++){
            try{
                MobileElement el8 = ad.findElementById("com.todoist:id/name");
                el8.sendKeys("SetelAutoTest");
                break;
            }
            catch(Exception e){
                System.out.print(e.getMessage());
            }
        }

        MobileElement el9 = ad.findElementByAccessibilityId("Done");
        el9.click();

    }
}
